#ifndef INTEL8080_CPU_H
#define INTEL8080_CPU_H
#include "memory.h"
#include "intel8080_regs.h"
#include <string>
#include "Types.h"
#include <QDebug>
class Intel8080_Cpu
{
public:
    Intel8080_Cpu(Memory &mem);
    ~Intel8080_Cpu(void);
    void Run(UINT32 cycles);
    void InterruptRequest(BYTE _opcode);
    void Input(BYTE data) { port1_in = data; port2_in = data & 0x70; }
    // BYTE Output(BYTE port_num) { return port_out[port_num]; }



private:
    enum OPERATION { _ADD, _SUB, _INR, _DCR, _OR, _AND, _XOR, _CMP };
    void FetchInstruction();
    void UpdateFlags(BYTE operand, WORD result, BYTE flags);
    void AuxiliaryCarry(BYTE operand1, BYTE operand2, OPERATION _operation );


    // MOVE, LOAD AND STORE
    void MOV();
    void MVI();
    void LXI();
    void STAX_B();
    void STAX_D();
    void LDAX_B();
    void LDAX_D();
    void STA();
    void LDA();
    void SHLD();
    void LHLD();
    void XCHG();
    //STACK OPS
    void PUSH();
    void POP();
    void XTHL();
    void SPHL();
    // JUMP
    void JMP();
    void JC();
    void JNC();
    void JZ();
    void JNZ();
    void JP();
    void JM();
    void JPE();
    void JPO();
    void PCHL();
    // CALL
    void CALL();
    void CC();
    void CNC();
    void CZ();
    void CNZ();
    void CP();
    void CM();
    void CPE();
    void CPO();
    // RETURN
    void RET();
    void RC();
    void RNC();
    void RZ();
    void RNZ();
    void RP();
    void RM();
    void RPE();
    void RPO();
    // RESTART
    void RST();
    // INCREMENT AND DECREMENT
    void INR();
    void DCR();
    void INX();
    void DCX();
    // ADD
    void ADD();
    void ADC();
    void ADI();
    void ACI();
    void DAD();
    // SUBTRACT
    void SUB();
    void SBB();
    void SUI();
    void SBI();
    // LOGICAL
    void ANA();
    void XRA();
    void ORA();
    void CMP();
    void ANI();
    void XRI();
    void ORI();
    void CPI();
    // ROTATE
    void RLC();
    void RRC();
    void RAL();
    void RAR();
    // SPECIALS
    void CMA();
    void STC();
    void CMC();
    void DAA();
    // INPUT/OUTPUT
    void IN();
    void OUT();
    // CONTROL
    void EI();
    void DI();
    void NOP();
    void HLT();


private:

    Memory& memory;
    Intel8080_Regs regs;
    bool interruptEnable;
    typedef void (Intel8080_Cpu::*INSTR_PTR)();
    enum { BC, DE, HL };
    BYTE opcode[3];
    BYTE port_in[256], port_out[256];
    BYTE port4_hi, port4_low, port2_out, port1_in,port2_in;

    //UINT32 emulatedCycles;
    struct Instruction
    {
        BYTE opcode, length, cycles;
        INSTR_PTR execute;
        std::string mnemonic;
        Instruction(){}
        Instruction(BYTE _opcode, BYTE _length, BYTE _cycles, INSTR_PTR _execute, std::string _mnemonic) : opcode(_opcode) , length(_length), cycles(_cycles), execute(_execute), mnemonic(_mnemonic) {}
    };

    Instruction instruction[256];

};

#endif // INTEL8080_CPU_H
