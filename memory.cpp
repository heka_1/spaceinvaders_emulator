#include "memory.h"

Memory::Memory(UINT32 size) : memorySize(size)
{
    memory = new BYTE[size];
    std::fill_n(memory,memorySize,0);

}


Memory::~Memory(void)
{
    delete[] memory;
}
BYTE Memory::ReadByte(WORD address) const
{

    return memory[address];
}

WORD Memory::ReadWord(WORD address) const
{
    return (memory[address + 1] << 8 | memory[address]);

}

void Memory::WriteByte(WORD address, BYTE data)
{

    if(address >= 0x2000) memory[address] = data;

}

void Memory::WriteWord(WORD address, WORD data)
{

    if(address >= 0x2000)
    {
        memory[address] = (BYTE)data;
        memory[address+1] = (BYTE)(data >> 8);
    }
    //std::cout << std::hex << (int)memory[address] <<std::endl;
    //std::cout << std::hex << (int)memory[address+1] << std::endl;

}
void Memory::LoadMemory(BYTE* data, WORD length)
{
    for(int i = 0; i < length; i++)
        memory[i] = data[i];

}
void Memory::LoadMemoryFromAddress(WORD address,BYTE* data, WORD length)
{
    for(int i = 0; i < length; i++)
        memory[address+i] = data[i];

}
void Memory::Dump()
{


    for(int i=0; i< memorySize ; i++)
    {
        std::cout << "address: " <<i ;
        std::cout << " data: " << std::hex << (int)memory[i] <<std::endl;


    }
}
void Memory::ClearVram()
{
    std::fill_n(&memory[0x2400],0x1c00,0x00);
}
