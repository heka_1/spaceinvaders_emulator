#ifndef EMULATOR_H
#define EMULATOR_H

#include <QObject>
#include <QDebug>
#include <QCoreApplication>
#include <QThread>
#include "memory.h"
#include "intel8080_cpu.h"
#include <fstream>
#include <QMessageBox>
#include <QIcon>
#include <QElapsedTimer>
#include <QTimer>
#include <QFile>



class Emulator : public QObject
{
    Q_OBJECT
public:
    explicit Emulator(QObject *parent = 0);
    
signals:
    void updateScreen(QByteArray);
public slots:
    void execute();
    void input(unsigned char in);
private:
    Memory memory;
    Intel8080_Cpu cpu;
    BYTE input_port1;
    bool running;


};

#endif // EMULATOR_H
