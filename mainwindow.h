#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QThread>
#include <QFocusEvent>
#include "emulator.h"
#include "screenwidget.h"
#include "keysdialog.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
signals:
    void output(unsigned char);
public slots:

    void onScale1x();
    void onScale2x();
    void displayKeysDialog();

protected:
    void keyPressEvent ( QKeyEvent *event );
    void keyReleaseEvent ( QKeyEvent *event );
    void focusOutEvent(QFocusEvent *event);
    void focusInEvent(QFocusEvent *event);
    void closeEvent(QCloseEvent *event);

private slots:
    void on_actionExit_triggered();

private:
    Ui::MainWindow *ui;
    ScreenWidget *screen;
    Emulator *emu;
    QThread *cpu_thread;
    BYTE keys;
};

#endif // MAINWINDOW_H
