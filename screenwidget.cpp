#include "screenwidget.h"

ScreenWidget::ScreenWidget(QWidget *parent) :
    QGLWidget(QGLFormat(QGL::SampleBuffers),parent), scale(1), screen_pixels(0x1bff,0xff)
{
    setFixedSize(224*2, 256*2);
    setAutoFillBackground(false);

}

void ScreenWidget::paintEvent(QPaintEvent *event)
{

    QPainter painter(this);
    QImage image((uchar*)(screen_pixels.constData()),256,224,QImage::Format_MonoLSB);

    image = image.scaled(scale*image.size().width(),scale*image.size().height());

    QSize currentSize(image.size());

    painter.translate(currentSize.height()/2,currentSize.width()/2);
    painter.rotate(270);
    painter.translate(-currentSize.width()/2,-currentSize.height()/2);
    painter.drawImage(0,0,image);



}



void ScreenWidget::onScale1x()
{
    this->setFixedSize(224,256);
    scale = 1;
    repaint();
}

void ScreenWidget::onScale2x()
{
    this->setFixedSize(224*2,256*2);
    scale = 2;
    repaint();
}

void ScreenWidget::updateScreen(QByteArray pixels)
{
    //qDebug() << pixels;
    screen_pixels = pixels;
   // repaint();
    update();

}
