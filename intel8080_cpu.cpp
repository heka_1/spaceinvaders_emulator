#include "intel8080_cpu.h"

Intel8080_Cpu::Intel8080_Cpu(Memory &mem) :
    memory(mem), port_in(), port_out()
{

    instruction[0x00] = Instruction(0x0,1,4,&Intel8080_Cpu::NOP,"NOP");
    instruction[0x01] = Instruction(0x1,3,10,&Intel8080_Cpu::LXI,"LXI B, ");
    instruction[0x02] = Instruction(0x2,1,7,&Intel8080_Cpu::STAX_B,"STAX B");
    instruction[0x03] = Instruction(0x3,1,5,&Intel8080_Cpu::INX,"INX B");
    instruction[0x04] = Instruction(0x4,1,5,&Intel8080_Cpu::INR,"INR B");
    instruction[0x05] = Instruction(0x5,1,5,&Intel8080_Cpu::DCR,"DCR B");
    instruction[0x06] = Instruction(0x6,2,7,&Intel8080_Cpu::MVI,"MVI B, ");
    instruction[0x07] = Instruction(0x7,1,4,&Intel8080_Cpu::RLC,"RLC");
    instruction[0x08] = Instruction(0x8,1,4,&Intel8080_Cpu::NOP,"NOP");
    instruction[0x09] = Instruction(0x9,1,10,&Intel8080_Cpu::DAD,"DAD B");
    instruction[0x0a] = Instruction(0xa,1,7,&Intel8080_Cpu::LDAX_B,"LDAX B");
    instruction[0x0b] = Instruction(0xb,1,5,&Intel8080_Cpu::DCX,"DCX B");
    instruction[0x0c] = Instruction(0xc,1,5,&Intel8080_Cpu::INR,"INR C");
    instruction[0x0d] = Instruction(0xd,1,5,&Intel8080_Cpu::DCR,"DCR C");
    instruction[0x0e] = Instruction(0xe,2,7,&Intel8080_Cpu::MVI,"MVI C, ");
    instruction[0x0f] = Instruction(0xf,1,4,&Intel8080_Cpu::RRC,"RRC");
    instruction[0x10] = Instruction(0x10,1,4,&Intel8080_Cpu::NOP,"NOP");
    instruction[0x11] = Instruction(0x11,3,10,&Intel8080_Cpu::LXI,"LXI");
    instruction[0x12] = Instruction(0x12,1,7,&Intel8080_Cpu::STAX_D,"STAX_D");
    instruction[0x13] = Instruction(0x13,1,5,&Intel8080_Cpu::INX,"INX");
    instruction[0x14] = Instruction(0x14,1,5,&Intel8080_Cpu::INR,"INR");
    instruction[0x15] = Instruction(0x15,1,5,&Intel8080_Cpu::DCR,"DCR");
    instruction[0x16] = Instruction(0x16,2,7,&Intel8080_Cpu::MVI,"MVI");
    instruction[0x17] = Instruction(0x17,1,4,&Intel8080_Cpu::RAL,"RAL");
    instruction[0x18] = Instruction(0x18,1,4,&Intel8080_Cpu::NOP,"NOP");
    instruction[0x19] = Instruction(0x19,1,10,&Intel8080_Cpu::DAD,"DAD");
    instruction[0x1a] = Instruction(0x1a,1,7,&Intel8080_Cpu::LDAX_D,"LDAX_D");
    instruction[0x1b] = Instruction(0x1b,1,5,&Intel8080_Cpu::DCX,"DCX");
    instruction[0x1c] = Instruction(0x1c,1,5,&Intel8080_Cpu::INR,"INR");
    instruction[0x1d] = Instruction(0x1d,1,5,&Intel8080_Cpu::DCR,"DCR");
    instruction[0x1e] = Instruction(0x1e,2,7,&Intel8080_Cpu::MVI,"MVI");
    instruction[0x1f] = Instruction(0x1f,1,4,&Intel8080_Cpu::RAR,"RAR");
    instruction[0x20] = Instruction(0x20,1,4,&Intel8080_Cpu::NOP,"NOP");
    instruction[0x21] = Instruction(0x21,3,10,&Intel8080_Cpu::LXI,"LXI");
    instruction[0x22] = Instruction(0x22,3,16,&Intel8080_Cpu::SHLD,"SHLD");
    instruction[0x23] = Instruction(0x23,1,5,&Intel8080_Cpu::INX,"INX");
    instruction[0x24] = Instruction(0x24,1,5,&Intel8080_Cpu::INR,"INR");
    instruction[0x25] = Instruction(0x25,1,5,&Intel8080_Cpu::DCR,"DCR");
    instruction[0x26] = Instruction(0x26,2,7,&Intel8080_Cpu::MVI,"MVI");
    instruction[0x27] = Instruction(0x27,1,4,&Intel8080_Cpu::DAA,"DAA");
    instruction[0x28] = Instruction(0x28,1,4,&Intel8080_Cpu::NOP,"NOP");
    instruction[0x29] = Instruction(0x29,1,10,&Intel8080_Cpu::DAD,"DAD");
    instruction[0x2a] = Instruction(0x2a,3,16,&Intel8080_Cpu::LHLD,"LHLD");
    instruction[0x2b] = Instruction(0x2b,1,5,&Intel8080_Cpu::DCX,"DCX");
    instruction[0x2c] = Instruction(0x2c,1,5,&Intel8080_Cpu::INR,"INR");
    instruction[0x2d] = Instruction(0x2d,1,5,&Intel8080_Cpu::DCR,"DCR");
    instruction[0x2e] = Instruction(0x2e,2,7,&Intel8080_Cpu::MVI,"MVI");
    instruction[0x2f] = Instruction(0x2f,1,4,&Intel8080_Cpu::CMA,"CMA");
    instruction[0x30] = Instruction(0x30,1,4,&Intel8080_Cpu::NOP,"NOP");
    instruction[0x31] = Instruction(0x31,3,10,&Intel8080_Cpu::LXI,"LXI");
    instruction[0x32] = Instruction(0x32,3,13,&Intel8080_Cpu::STA,"STA");
    instruction[0x33] = Instruction(0x33,1,5,&Intel8080_Cpu::INX,"INX");
    instruction[0x34] = Instruction(0x34,1,10,&Intel8080_Cpu::INR,"INR");
    instruction[0x35] = Instruction(0x35,1,10,&Intel8080_Cpu::DCR,"DCR");
    instruction[0x36] = Instruction(0x36,2,10,&Intel8080_Cpu::MVI,"MVI");
    instruction[0x37] = Instruction(0x37,1,4,&Intel8080_Cpu::STC,"STC");
    instruction[0x38] = Instruction(0x38,1,4,&Intel8080_Cpu::NOP,"NOP");
    instruction[0x39] = Instruction(0x39,1,10,&Intel8080_Cpu::DAD,"DAD");
    instruction[0x3a] = Instruction(0x3a,3,13,&Intel8080_Cpu::LDA,"LDA");
    instruction[0x3b] = Instruction(0x3b,1,5,&Intel8080_Cpu::DCX,"DCX");
    instruction[0x3c] = Instruction(0x3c,1,5,&Intel8080_Cpu::INR,"INR");
    instruction[0x3d] = Instruction(0x3d,1,5,&Intel8080_Cpu::DCR,"DCR");
    instruction[0x3e] = Instruction(0x3e,2,7,&Intel8080_Cpu::MVI,"MVI");
    instruction[0x3f] = Instruction(0x3f,1,4,&Intel8080_Cpu::CMC,"CMC");
    instruction[0x40] = Instruction(0x40,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x41] = Instruction(0x41,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x42] = Instruction(0x42,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x43] = Instruction(0x43,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x44] = Instruction(0x44,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x45] = Instruction(0x45,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x46] = Instruction(0x46,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x47] = Instruction(0x47,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x48] = Instruction(0x48,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x49] = Instruction(0x49,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x4a] = Instruction(0x4a,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x4b] = Instruction(0x4b,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x4c] = Instruction(0x4c,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x4d] = Instruction(0x4d,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x4e] = Instruction(0x4e,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x4f] = Instruction(0x4f,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x50] = Instruction(0x50,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x51] = Instruction(0x51,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x52] = Instruction(0x52,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x53] = Instruction(0x53,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x54] = Instruction(0x54,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x55] = Instruction(0x55,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x56] = Instruction(0x56,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x57] = Instruction(0x57,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x58] = Instruction(0x58,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x59] = Instruction(0x59,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x5a] = Instruction(0x5a,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x5b] = Instruction(0x5b,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x5c] = Instruction(0x5c,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x5d] = Instruction(0x5d,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x5e] = Instruction(0x5e,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x5f] = Instruction(0x5f,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x60] = Instruction(0x60,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x61] = Instruction(0x61,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x62] = Instruction(0x62,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x63] = Instruction(0x63,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x64] = Instruction(0x64,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x65] = Instruction(0x65,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x66] = Instruction(0x66,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x67] = Instruction(0x67,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x68] = Instruction(0x68,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x69] = Instruction(0x69,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x6a] = Instruction(0x6a,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x6b] = Instruction(0x6b,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x6c] = Instruction(0x6c,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x6d] = Instruction(0x6d,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x6e] = Instruction(0x6e,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x6f] = Instruction(0x6f,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x70] = Instruction(0x70,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x71] = Instruction(0x71,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x72] = Instruction(0x72,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x73] = Instruction(0x73,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x74] = Instruction(0x74,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x75] = Instruction(0x75,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x76] = Instruction(0x76,1,7,&Intel8080_Cpu::HLT,"HLT");
    instruction[0x77] = Instruction(0x77,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x78] = Instruction(0x78,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x79] = Instruction(0x79,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x7a] = Instruction(0x7a,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x7b] = Instruction(0x7b,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x7c] = Instruction(0x7c,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x7d] = Instruction(0x7d,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x7e] = Instruction(0x7e,1,7,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x7f] = Instruction(0x7f,1,5,&Intel8080_Cpu::MOV,"MOV");
    instruction[0x80] = Instruction(0x80,1,4,&Intel8080_Cpu::ADD,"ADD");
    instruction[0x81] = Instruction(0x81,1,4,&Intel8080_Cpu::ADD,"ADD");
    instruction[0x82] = Instruction(0x82,1,4,&Intel8080_Cpu::ADD,"ADD");
    instruction[0x83] = Instruction(0x83,1,4,&Intel8080_Cpu::ADD,"ADD");
    instruction[0x84] = Instruction(0x84,1,4,&Intel8080_Cpu::ADD,"ADD");
    instruction[0x85] = Instruction(0x85,1,4,&Intel8080_Cpu::ADD,"ADD");
    instruction[0x86] = Instruction(0x86,1,7,&Intel8080_Cpu::ADD,"ADD");
    instruction[0x87] = Instruction(0x87,1,4,&Intel8080_Cpu::ADD,"ADD");
    instruction[0x88] = Instruction(0x88,1,4,&Intel8080_Cpu::ADC,"ADC");
    instruction[0x89] = Instruction(0x89,1,4,&Intel8080_Cpu::ADC,"ADC");
    instruction[0x8a] = Instruction(0x8a,1,4,&Intel8080_Cpu::ADC,"ADC");
    instruction[0x8b] = Instruction(0x8b,1,4,&Intel8080_Cpu::ADC,"ADC");
    instruction[0x8c] = Instruction(0x8c,1,4,&Intel8080_Cpu::ADC,"ADC");
    instruction[0x8d] = Instruction(0x8d,1,4,&Intel8080_Cpu::ADC,"ADC");
    instruction[0x8e] = Instruction(0x8e,1,7,&Intel8080_Cpu::ADC,"ADC");
    instruction[0x8f] = Instruction(0x8f,1,4,&Intel8080_Cpu::ADC,"ADC");
    instruction[0x90] = Instruction(0x90,1,4,&Intel8080_Cpu::SUB,"SUB");
    instruction[0x91] = Instruction(0x91,1,4,&Intel8080_Cpu::SUB,"SUB");
    instruction[0x92] = Instruction(0x92,1,4,&Intel8080_Cpu::SUB,"SUB");
    instruction[0x93] = Instruction(0x93,1,4,&Intel8080_Cpu::SUB,"SUB");
    instruction[0x94] = Instruction(0x94,1,4,&Intel8080_Cpu::SUB,"SUB");
    instruction[0x95] = Instruction(0x95,1,4,&Intel8080_Cpu::SUB,"SUB");
    instruction[0x96] = Instruction(0x96,1,7,&Intel8080_Cpu::SUB,"SUB");
    instruction[0x97] = Instruction(0x97,1,4,&Intel8080_Cpu::SUB,"SUB");
    instruction[0x98] = Instruction(0x98,1,4,&Intel8080_Cpu::SBB,"SBB");
    instruction[0x99] = Instruction(0x99,1,4,&Intel8080_Cpu::SBB,"SBB");
    instruction[0x9a] = Instruction(0x9a,1,4,&Intel8080_Cpu::SBB,"SBB");
    instruction[0x9b] = Instruction(0x9b,1,4,&Intel8080_Cpu::SBB,"SBB");
    instruction[0x9c] = Instruction(0x9c,1,4,&Intel8080_Cpu::SBB,"SBB");
    instruction[0x9d] = Instruction(0x9d,1,4,&Intel8080_Cpu::SBB,"SBB");
    instruction[0x9e] = Instruction(0x9e,1,7,&Intel8080_Cpu::SBB,"SBB");
    instruction[0x9f] = Instruction(0x9f,1,4,&Intel8080_Cpu::SBB,"SBB");
    instruction[0xa0] = Instruction(0xa0,1,4,&Intel8080_Cpu::ANA,"ANA");
    instruction[0xa1] = Instruction(0xa1,1,4,&Intel8080_Cpu::ANA,"ANA");
    instruction[0xa2] = Instruction(0xa2,1,4,&Intel8080_Cpu::ANA,"ANA");
    instruction[0xa3] = Instruction(0xa3,1,4,&Intel8080_Cpu::ANA,"ANA");
    instruction[0xa4] = Instruction(0xa4,1,4,&Intel8080_Cpu::ANA,"ANA");
    instruction[0xa5] = Instruction(0xa5,1,4,&Intel8080_Cpu::ANA,"ANA");
    instruction[0xa6] = Instruction(0xa6,1,7,&Intel8080_Cpu::ANA,"ANA");
    instruction[0xa7] = Instruction(0xa7,1,4,&Intel8080_Cpu::ANA,"ANA");
    instruction[0xa8] = Instruction(0xa8,1,4,&Intel8080_Cpu::XRA,"XRA");
    instruction[0xa9] = Instruction(0xa9,1,4,&Intel8080_Cpu::XRA,"XRA");
    instruction[0xaa] = Instruction(0xaa,1,4,&Intel8080_Cpu::XRA,"XRA");
    instruction[0xab] = Instruction(0xab,1,4,&Intel8080_Cpu::XRA,"XRA");
    instruction[0xac] = Instruction(0xac,1,4,&Intel8080_Cpu::XRA,"XRA");
    instruction[0xad] = Instruction(0xad,1,4,&Intel8080_Cpu::XRA,"XRA");
    instruction[0xae] = Instruction(0xae,1,7,&Intel8080_Cpu::XRA,"XRA");
    instruction[0xaf] = Instruction(0xaf,1,4,&Intel8080_Cpu::XRA,"XRA");
    instruction[0xb0] = Instruction(0xb0,1,4,&Intel8080_Cpu::ORA,"ORA");
    instruction[0xb1] = Instruction(0xb1,1,4,&Intel8080_Cpu::ORA,"ORA");
    instruction[0xb2] = Instruction(0xb2,1,4,&Intel8080_Cpu::ORA,"ORA");
    instruction[0xb3] = Instruction(0xb3,1,4,&Intel8080_Cpu::ORA,"ORA");
    instruction[0xb4] = Instruction(0xb4,1,4,&Intel8080_Cpu::ORA,"ORA");
    instruction[0xb5] = Instruction(0xb5,1,4,&Intel8080_Cpu::ORA,"ORA");
    instruction[0xb6] = Instruction(0xb6,1,7,&Intel8080_Cpu::ORA,"ORA");
    instruction[0xb7] = Instruction(0xb7,1,4,&Intel8080_Cpu::ORA,"ORA");
    instruction[0xb8] = Instruction(0xb8,1,4,&Intel8080_Cpu::CMP,"CMP");
    instruction[0xb9] = Instruction(0xb9,1,4,&Intel8080_Cpu::CMP,"CMP");
    instruction[0xba] = Instruction(0xba,1,4,&Intel8080_Cpu::CMP,"CMP");
    instruction[0xbb] = Instruction(0xbb,1,4,&Intel8080_Cpu::CMP,"CMP");
    instruction[0xbc] = Instruction(0xbc,1,4,&Intel8080_Cpu::CMP,"CMP");
    instruction[0xbd] = Instruction(0xbd,1,4,&Intel8080_Cpu::CMP,"CMP");
    instruction[0xbe] = Instruction(0xbe,1,7,&Intel8080_Cpu::CMP,"CMP");
    instruction[0xbf] = Instruction(0xbf,1,4,&Intel8080_Cpu::CMP,"CMP");
    instruction[0xc0] = Instruction(0xc0,1,5,&Intel8080_Cpu::RNZ,"RNZ");
    instruction[0xc1] = Instruction(0xc1,1,10,&Intel8080_Cpu::POP,"POP");
    instruction[0xc2] = Instruction(0xc2,3,10,&Intel8080_Cpu::JNZ,"JNZ");
    instruction[0xc3] = Instruction(0xc3,3,10,&Intel8080_Cpu::JMP,"JMP");
    instruction[0xc4] = Instruction(0xc4,3,11,&Intel8080_Cpu::CNZ,"CNZ");
    instruction[0xc5] = Instruction(0xc5,1,11,&Intel8080_Cpu::PUSH,"PUSH");
    instruction[0xc6] = Instruction(0xc6,2,7,&Intel8080_Cpu::ADI,"ADI");
    instruction[0xc7] = Instruction(0xc7,1,11,&Intel8080_Cpu::RST,"RST");
    instruction[0xc8] = Instruction(0xc8,1,5,&Intel8080_Cpu::RZ,"RZ");
    instruction[0xc9] = Instruction(0xc9,1,10,&Intel8080_Cpu::RET,"RET");
    instruction[0xca] = Instruction(0xca,3,10,&Intel8080_Cpu::JZ,"JZ");
    instruction[0xcb] = Instruction(0xcb,3,10,&Intel8080_Cpu::JMP,"JMP");
    instruction[0xcc] = Instruction(0xcc,3,11,&Intel8080_Cpu::CZ,"CZ");
    instruction[0xcd] = Instruction(0xcd,3,17,&Intel8080_Cpu::CALL,"CALL");
    instruction[0xce] = Instruction(0xce,2,7,&Intel8080_Cpu::ACI,"ACI");
    instruction[0xcf] = Instruction(0xcf,1,11,&Intel8080_Cpu::RST,"RST");
    instruction[0xd0] = Instruction(0xd0,1,5,&Intel8080_Cpu::RNC,"RNC");
    instruction[0xd1] = Instruction(0xd1,1,10,&Intel8080_Cpu::POP,"POP");
    instruction[0xd2] = Instruction(0xd2,3,10,&Intel8080_Cpu::JNC,"JNC");
    instruction[0xd3] = Instruction(0xd3,2,10,&Intel8080_Cpu::OUT,"OUT");
    instruction[0xd4] = Instruction(0xd4,3,11,&Intel8080_Cpu::CNC,"CNC");
    instruction[0xd5] = Instruction(0xd5,1,11,&Intel8080_Cpu::PUSH,"PUSH");
    instruction[0xd6] = Instruction(0xd6,2,7,&Intel8080_Cpu::SUI,"SUI");
    instruction[0xd7] = Instruction(0xd7,1,11,&Intel8080_Cpu::RST,"RST");
    instruction[0xd8] = Instruction(0xd8,1,5,&Intel8080_Cpu::RC,"RC");
    instruction[0xd9] = Instruction(0xd9,1,10,&Intel8080_Cpu::RET,"RET");
    instruction[0xda] = Instruction(0xda,3,10,&Intel8080_Cpu::JC,"JC");
    instruction[0xdb] = Instruction(0xdb,2,10,&Intel8080_Cpu::IN,"IN");
    instruction[0xdc] = Instruction(0xdc,3,11,&Intel8080_Cpu::CC,"CC");
    instruction[0xdd] = Instruction(0xdd,3,17,&Intel8080_Cpu::CALL,"CALL");
    instruction[0xde] = Instruction(0xde,2,7,&Intel8080_Cpu::SBI,"SBI");
    instruction[0xdf] = Instruction(0xdf,1,11,&Intel8080_Cpu::RST,"RST");
    instruction[0xe0] = Instruction(0xe0,1,5,&Intel8080_Cpu::RPO,"RPO");
    instruction[0xe1] = Instruction(0xe1,1,10,&Intel8080_Cpu::POP,"POP");
    instruction[0xe2] = Instruction(0xe2,3,10,&Intel8080_Cpu::JPO,"JPO");
    instruction[0xe3] = Instruction(0xe3,1,18,&Intel8080_Cpu::XTHL,"XTHL");
    instruction[0xe4] = Instruction(0xe4,3,11,&Intel8080_Cpu::CPO,"CPO");
    instruction[0xe5] = Instruction(0xe5,1,11,&Intel8080_Cpu::PUSH,"PUSH");
    instruction[0xe6] = Instruction(0xe6,2,7,&Intel8080_Cpu::ANI,"ANI");
    instruction[0xe7] = Instruction(0xe7,1,11,&Intel8080_Cpu::RST,"RST");
    instruction[0xe8] = Instruction(0xe8,1,5,&Intel8080_Cpu::RPE,"RPE");
    instruction[0xe9] = Instruction(0xe9,1,5,&Intel8080_Cpu::PCHL,"PCHL");
    instruction[0xea] = Instruction(0xea,3,10,&Intel8080_Cpu::JPE,"JPE");
    instruction[0xeb] = Instruction(0xeb,1,5,&Intel8080_Cpu::XCHG,"XCHG");
    instruction[0xec] = Instruction(0xec,3,11,&Intel8080_Cpu::CPE,"CPE");
    instruction[0xed] = Instruction(0xed,3,17,&Intel8080_Cpu::CALL,"CALL");
    instruction[0xee] = Instruction(0xee,2,7,&Intel8080_Cpu::XRI,"XRI");
    instruction[0xef] = Instruction(0xef,1,11,&Intel8080_Cpu::RST,"RST");
    instruction[0xf0] = Instruction(0xf0,1,5,&Intel8080_Cpu::RP,"RP");
    instruction[0xf1] = Instruction(0xf1,1,10,&Intel8080_Cpu::POP,"POP");
    instruction[0xf2] = Instruction(0xf2,3,10,&Intel8080_Cpu::JP,"JP");
    instruction[0xf3] = Instruction(0xf3,1,4,&Intel8080_Cpu::DI,"DI");
    instruction[0xf4] = Instruction(0xf4,3,11,&Intel8080_Cpu::CP,"CP");
    instruction[0xf5] = Instruction(0xf5,1,11,&Intel8080_Cpu::PUSH,"PUSH");
    instruction[0xf6] = Instruction(0xf6,2,7,&Intel8080_Cpu::ORI,"ORI");
    instruction[0xf7] = Instruction(0xf7,1,11,&Intel8080_Cpu::RST,"RST");
    instruction[0xf8] = Instruction(0xf8,1,5,&Intel8080_Cpu::RM,"RM");
    instruction[0xf9] = Instruction(0xf9,1,5,&Intel8080_Cpu::SPHL,"SPHL");
    instruction[0xfa] = Instruction(0xfa,3,10,&Intel8080_Cpu::JM,"JM");
    instruction[0xfb] = Instruction(0xfb,1,4,&Intel8080_Cpu::EI,"EI");
    instruction[0xfc] = Instruction(0xfc,3,11,&Intel8080_Cpu::CM,"CM");
    instruction[0xfd] = Instruction(0xfd,3,17,&Intel8080_Cpu::CALL,"CALL");
    instruction[0xfe] = Instruction(0xfe,2,7,&Intel8080_Cpu::CPI,"CPI");
    instruction[0xff] = Instruction(0xff,1,11,&Intel8080_Cpu::RST,"RST");

    regs.PC = 0x00;


}
Intel8080_Cpu::~Intel8080_Cpu(void)
{

}
void Intel8080_Cpu::Run(UINT32 cycles)
{
    UINT32 emulatedCycles = 0;


    while(emulatedCycles < cycles)
    {

        FetchInstruction();


        (this->*instruction[opcode[0]].execute)();



        emulatedCycles += instruction[opcode[0]].cycles;
    }

}

void Intel8080_Cpu::InterruptRequest(BYTE _opcode)
{

    if(interruptEnable)
    {
        interruptEnable = false;

        opcode[0] =  _opcode;
        (this->*instruction[_opcode].execute)();


    }
}

void Intel8080_Cpu::FetchInstruction()
{


    opcode[0] = memory.ReadByte(regs.PC++);

    if( instruction[opcode[0]].length == 2)
    {
        opcode[1] = memory.ReadByte(regs.PC++);
    }
    else if(instruction[opcode[0]].length == 3)
    {
        opcode[1] = memory.ReadByte(regs.PC++);
        opcode[2] = memory.ReadByte(regs.PC++);
    }


}

void Intel8080_Cpu::UpdateFlags(BYTE operand, WORD result, BYTE flags)
{

    if((flags & ZERO) != 0 )
    {
        regs.SetFlag(ZERO, ((result & 0xFF)  == 0) );
    }

    if((flags & SIGN) != 0 )
    {
        regs.SetFlag(SIGN, ((result & SIGN) == SIGN));
    }

    if((flags & PARITY) != 0 )
    {
        BYTE count = 0;
        for(int i = 0; i <=7; i++)
            if(result & 1 << i) count++;

        if(count % 2 == 0)
        {
            regs.F |= PARITY;
        }
        else regs.F &=  (~(PARITY));
    }

    if((flags & CARRY) != 0 )
    {
        regs.SetFlag(CARRY, (result > 0xFF));
    }


    /*  if((flags & AUXCARRY) != 0 )
        {

            BYTE bit4_before = (operand & AUXCARRY);
            BYTE bit4 = (result & AUXCARRY);

            BYTE bit3_before = operand & 0x08;
            BYTE bit3 = result & 0x08;

            regs.SetFlag(AUXCARRY, (bit4 != bit4_before && bit3 != bit3_before));

        }
*/

}

void Intel8080_Cpu::AuxiliaryCarry(BYTE operand1, BYTE operand2, Intel8080_Cpu::OPERATION operation)
{

    switch(operation)
    {
    case _ADD:
        regs.SetFlag(AUXCARRY, ((operand1 & 0x0f) + (operand2 & 0x0f)) > 15  );

        break;
    case _SUB:
        regs.SetFlag(AUXCARRY, ((operand1 & 0x0f) - (operand2 & 0x0f)) > 15  );
        break;
    case _INR:
        regs.SetFlag(AUXCARRY, ((operand1 & 0x0f) + (operand2 & 0x0f)) > 15  );
        break;
    case _DCR:
        regs.SetFlag(AUXCARRY, ((operand1 & 0x0f) - (operand2 & 0x0f)) > 15  );
        break;
    case _OR:
        regs.SetFlag(AUXCARRY, ((operand1 & 0x0f) | (operand2 & 0x0f)) > 15  );
        break;
    case _AND:
        regs.SetFlag(AUXCARRY, ((operand1 & 0x0f) & (operand2 & 0x0f)) > 15  );
        break;
    case _XOR:
        regs.SetFlag(AUXCARRY, ((operand1 & 0x0f) ^ (operand2 & 0x0f)) > 15  );
        break;
    case _CMP:
        regs.SetFlag(AUXCARRY, ((operand1 & 0x0f) - (operand2 & 0x0f)) > 15  );
        break;


    }
}

// MOVE, LOAD AND STORE
void Intel8080_Cpu::MOV()
{
    BYTE dest = opcode[0] >> 3 & 0x07;
    BYTE src = opcode[0] & 0x07;

    if (dest == 0x06 && src != 0x06) // MOV M, r
    {
        memory.WriteByte(regs.GetRegisterPair(HL), regs[src]);
    }
    else if (dest != 0x06 && src == 0x06) // MOV r,M
    {
        regs[dest] = memory.ReadByte( regs.GetRegisterPair(HL));
    }
    if (dest != 0x06 && src != 0x06) // MOV r,r
    {
        regs[dest] = regs[src];

    }


}
void Intel8080_Cpu::MVI()
{
    BYTE dest = opcode[0] >> 3 & 0x07;

    if (dest == 0x06)
        memory.WriteByte(regs.GetRegisterPair(HL), opcode[1]);
    else
        regs[dest] = opcode[1];


}
void Intel8080_Cpu::LXI()
{
    // register pairs  0=BC, 1=DE, 2=HL, 3=SP
    int registerPair = ((opcode[0] & 0x30) >> 4);


    if (registerPair == 3)
    {
        regs.SP = opcode[2] << 8 | opcode[1];
    }
    else
    {
        regs.SetRegisterPair(registerPair,opcode[2] << 8 | opcode[1]);
    }
}
void Intel8080_Cpu::STAX_B()
{
    WORD address = regs.GetRegisterPair(BC);
    memory.WriteByte(address, regs.A);
}
void Intel8080_Cpu::STAX_D()
{
    WORD address = regs.GetRegisterPair(DE);
    memory.WriteByte(address, regs.A);
}
void Intel8080_Cpu::LDAX_B()
{
    WORD address = regs.GetRegisterPair(BC);
    regs.A = memory.ReadByte(address);
}
void Intel8080_Cpu::LDAX_D()
{
    WORD address = regs.GetRegisterPair(DE);
    regs.A = memory.ReadByte(address);
}
void Intel8080_Cpu::STA()
{
    WORD address = opcode[2] << 8 | opcode[1];
    memory.WriteByte(address, regs.A);
}
void Intel8080_Cpu::LDA()
{
    WORD address = opcode[2] << 8 | opcode[1];
    regs.A = memory.ReadByte(address);
}
void Intel8080_Cpu::SHLD()
{
    WORD address = opcode[2] << 8 | opcode[1];
    WORD data = regs.GetRegisterPair(HL);
    memory.WriteWord(address,data);

}
void Intel8080_Cpu::LHLD()
{
    WORD address = opcode[2] << 8 | opcode[1];
    regs.SetRegisterPair(HL, memory.ReadWord(address));

}
void Intel8080_Cpu::XCHG()
{
    BYTE tmp = regs.H;
    regs.H = regs.D;
    regs.D = tmp;

    tmp = regs.L;
    regs.L = regs.E;
    regs.E = tmp;
}
//STACK OPS
void Intel8080_Cpu::PUSH()
{
    int registerPair = ((opcode[0] & 0x30) >> 4);

    if (registerPair == 0x03)
    {
        memory.WriteByte(regs.SP - 1, regs.A);
        memory.WriteByte(regs.SP - 2, regs.F);
    }
    else
    {
        WORD rp = regs.GetRegisterPair(registerPair);
        memory.WriteByte(regs.SP - 1, (BYTE)(rp >> 8) );
        memory.WriteByte(regs.SP - 2, (BYTE)(rp) );
    }

    regs.SP -= 2;
}
void Intel8080_Cpu::POP()
{
    int registerPair = ((opcode[0] & 0x30) >> 4);

    if (registerPair == 0x03)
    {
        regs.A =  memory.ReadByte(regs.SP + 1);
        regs.F = memory.ReadByte(regs.SP);
    }
    else
    {
        regs.SetRegisterPair(registerPair, memory.ReadWord(regs.SP));
    }

    regs.SP += 2;
}
void Intel8080_Cpu::XTHL()
{
    BYTE tmp = regs.H;
    regs.H = memory.ReadByte(regs.SP + 1);
    memory.WriteByte(regs.SP + 1, tmp);

    tmp = regs.L;
    regs.L = memory.ReadByte(regs.SP);
    memory.WriteByte(regs.SP, tmp);
}
void Intel8080_Cpu::SPHL()
{
    regs.SP = regs.GetRegisterPair(HL);
}
// JUMP
void Intel8080_Cpu::JMP()
{
    regs.PC = opcode[2] << 8 | opcode[1];
}
void Intel8080_Cpu::JC()
{
    if(regs.IsFlagSet(CARRY) == true)
        JMP();

}
void Intel8080_Cpu::JNC()
{
    if(regs.IsFlagSet(CARRY) == false)
        JMP();
}
void Intel8080_Cpu::JZ()
{
    if(regs.IsFlagSet(ZERO) == true)
        JMP();
}
void Intel8080_Cpu::JNZ()
{
    if(regs.IsFlagSet(ZERO) == false)
        JMP();
}
void Intel8080_Cpu::JP()
{
    if(regs.IsFlagSet(SIGN) == false)
        JMP();
}
void Intel8080_Cpu::JM()
{
    if(regs.IsFlagSet(SIGN) == true)
        JMP();
}
void Intel8080_Cpu::JPE()
{
    if(regs.IsFlagSet(PARITY) == true)
        JMP();
}
void Intel8080_Cpu::JPO()
{
    if(regs.IsFlagSet(PARITY) == false)
        JMP();
}
void Intel8080_Cpu::PCHL()
{
    regs.PC = regs.GetRegisterPair(HL);
}
// CALL
void Intel8080_Cpu::CALL()
{

    memory.WriteByte(regs.SP - 1, (BYTE)(regs.PC >> 8));
    memory.WriteByte(regs.SP - 2, (BYTE)(regs.PC ));
    regs.SP-=2;

    regs.PC = opcode[2] << 8 | opcode[1];
}
void Intel8080_Cpu::CC()
{
    if (regs.IsFlagSet(CARRY) == true)
        CALL();
}
void Intel8080_Cpu::CNC()
{
    if (regs.IsFlagSet(CARRY) == false)
        CALL();
}
void Intel8080_Cpu::CZ()
{
    if (regs.IsFlagSet(ZERO) == true)
        CALL();
}
void Intel8080_Cpu::CNZ()
{
    if (regs.IsFlagSet(ZERO) == false)
        CALL();
}
void Intel8080_Cpu::CP()
{
    if (regs.IsFlagSet(SIGN) == false)
        CALL();
}
void Intel8080_Cpu::CM()
{
    if (regs.IsFlagSet(SIGN) == true)
        CALL();
}
void Intel8080_Cpu::CPE()
{
    if (regs.IsFlagSet(PARITY) == true)
        CALL();
}
void Intel8080_Cpu::CPO()
{
    if (regs.IsFlagSet(PARITY) == false)
        CALL();
}
// RETURN
void Intel8080_Cpu::RET()
{
    BYTE PCl = memory.ReadByte(regs.SP);
    BYTE PCh = memory.ReadByte(regs.SP+1);
    regs.PC = PCh << 8 | PCl;

    regs.SP += 2;
}
void Intel8080_Cpu::RC()
{
    if(regs.IsFlagSet(CARRY) == true)
        RET();
}
void Intel8080_Cpu::RNC()
{
    if(regs.IsFlagSet(CARRY) == false)
        RET();
}
void Intel8080_Cpu::RZ()
{
    if(regs.IsFlagSet(ZERO) == true)
        RET();
}
void Intel8080_Cpu::RNZ()
{
    if(regs.IsFlagSet(ZERO) == false)
        RET();
}
void Intel8080_Cpu::RP()
{
    if(regs.IsFlagSet(SIGN) == false)
        RET();
}
void Intel8080_Cpu::RM()
{
    if(regs.IsFlagSet(SIGN) == true)
        RET();
}
void Intel8080_Cpu::RPE()
{
    if(regs.IsFlagSet(PARITY) == true)
        RET();
}
void Intel8080_Cpu::RPO()
{
    if(regs.IsFlagSet(PARITY) == false)
        RET();
}
// RESTART
void Intel8080_Cpu::RST()
{
    BYTE PCl = (BYTE)regs.PC;
    BYTE PCh = (BYTE)(regs.PC >> 8);

    memory.WriteByte(regs.SP-1,PCh);
    memory.WriteByte(regs.SP-2,PCl);

    regs.SP -= 2;

    regs.PC = (opcode[0] & 0x38);
}
// INCREMENT AND DECREMENT
void Intel8080_Cpu::INR()
{
    BYTE dst =((opcode[0] & 0x38) >> 3);
    if (dst == 0x06) // INR M
    {
        WORD address = regs.GetRegisterPair(HL);
        BYTE tmp = memory.ReadByte(address);

        UpdateFlags(tmp, tmp + 1,SIGN | ZERO | PARITY);
        AuxiliaryCarry(tmp,1,_INR);
        tmp++;
        memory.WriteByte(address, tmp);
    }
    else // INR r
    {
        UpdateFlags(regs[dst], regs[dst] + 1, SIGN | ZERO  | PARITY);
        AuxiliaryCarry(regs[dst],1,_INR);
        regs[dst]++;
    }
}
void Intel8080_Cpu::DCR()
{
    BYTE dst =((opcode[0] & 0x38) >> 3);
    if (dst == 0x06) // DCR M
    {
        WORD address = regs.GetRegisterPair(HL);
        BYTE tmp = memory.ReadByte(address);
        UpdateFlags(tmp,tmp-1 ,SIGN | ZERO | PARITY);
        AuxiliaryCarry(tmp,1,_DCR);
        tmp--;
        memory.WriteByte(address, tmp);
    }
    else // DCR r
    {
        UpdateFlags(regs[dst],regs[dst] - 1, SIGN | ZERO | PARITY);
        AuxiliaryCarry(regs[dst],1,_DCR);
        regs[dst]--;
    }
}
void Intel8080_Cpu::INX()
{
    int registerPair = ((opcode[0] & 0x30) >> 4);

    if (registerPair == 0x03)
    {
        regs.SP++;
    }
    else
    {
        regs.SetRegisterPair(registerPair, regs.GetRegisterPair(registerPair) + 1 );
    }


}
void Intel8080_Cpu::DCX()
{
    int registerPair = ((opcode[0] & 0x30) >> 4);

    if (registerPair == 0x03)
    {
        regs.SP--;
    }
    else
    {
        regs.SetRegisterPair(registerPair, regs.GetRegisterPair(registerPair) - 1 );
    }
}
// ADD
void Intel8080_Cpu::ADD()
{
    BYTE src = opcode[0] & 0x07;
    if (src == 0x06)
    {
        BYTE data = memory.ReadByte(regs.GetRegisterPair(HL));
        UpdateFlags(regs.A, regs.A + data, SIGN | ZERO  | PARITY | CARRY);
        AuxiliaryCarry(regs.A,data,_ADD);
        regs.A += data;
    }
    else
    {
        UpdateFlags(regs.A, regs.A + regs[src], SIGN | ZERO  | PARITY | CARRY);
        AuxiliaryCarry(regs.A,regs[src],_ADD);
        regs.A += regs[src];
    }
}
void Intel8080_Cpu::ADC()
{
    BYTE src =((opcode[0] & 0x07));
    BYTE carry = regs.IsFlagSet(CARRY) ? 1 : 0;

    if (src == 0x06)
    {
        BYTE data = memory.ReadByte(regs.GetRegisterPair(HL));
        UpdateFlags(regs.A, regs.A + data + carry, SIGN | ZERO | PARITY | CARRY);
        AuxiliaryCarry(regs.A,data+carry,_ADD);
        regs.A += data + carry;
    }
    else
    {
        UpdateFlags(regs.A, regs.A + regs[src] + carry, SIGN | ZERO | PARITY | CARRY);
        AuxiliaryCarry(regs.A,regs[src]+carry,_ADD);
        regs.A += regs[src] + carry;
    }
}
void Intel8080_Cpu::ADI()
{

    UpdateFlags(regs.A, regs.A + opcode[1], SIGN | ZERO | PARITY | CARRY);
    AuxiliaryCarry(regs.A,opcode[1],_ADD);
    regs.A += opcode[1];
}
void Intel8080_Cpu::ACI()
{

    BYTE carry = regs.F & CARRY;
    UpdateFlags(regs.A, regs.A + opcode[1] + carry, SIGN | ZERO | PARITY | CARRY);
    AuxiliaryCarry(regs.A,opcode[1]+carry,_ADD);
    regs.A += opcode[1] + carry;
}
void Intel8080_Cpu::DAD()
{
    int registerPair = ((opcode[0] & 0x30) >> 4);

    if (registerPair == 0x03)
    {
        UINT32 tmp = regs.GetRegisterPair(HL) + regs.SP;
        regs.SetFlag(CARRY, (tmp > 0xFFFF));
        regs.SetRegisterPair(HL, tmp);
    }
    else
    {
        UINT32 tmp = regs.GetRegisterPair(HL) + regs.GetRegisterPair(registerPair);
        regs.SetFlag(CARRY, (tmp > 0xFFFF));
        regs.SetRegisterPair(HL, tmp);
    }
}
// SUBTRACT
void Intel8080_Cpu::SUB()
{
    BYTE src =((opcode[0] & 0x07));
    if (src == 0x06)
    {
        BYTE data = memory.ReadByte(regs.GetRegisterPair(HL));
        UpdateFlags(regs.A, regs.A - data, SIGN | ZERO | PARITY | CARRY);
        AuxiliaryCarry(regs.A,data,_SUB);
        regs.A -= data;
    }
    else
    {
        UpdateFlags(regs.A, regs.A - regs[src], SIGN | ZERO | PARITY | CARRY);
        AuxiliaryCarry(regs.A,regs[src],_SUB);
        regs.A -= regs[src];
    }
}
void Intel8080_Cpu::SBB()
{
    BYTE src =((opcode[0] & 0x07));
    BYTE carry = regs.IsFlagSet(CARRY) ? 1 : 0;

    if (src == 0x06)
    {
        BYTE data = memory.ReadByte(regs.GetRegisterPair(HL));
        UpdateFlags(regs.A, regs.A - data - carry, SIGN | ZERO | PARITY | CARRY);
        AuxiliaryCarry(regs.A,data-carry,_SUB);
        regs.A -= data - carry;
    }
    else
    {
        UpdateFlags(regs.A, regs.A - regs[src] - carry, SIGN | ZERO  | PARITY | CARRY);
        AuxiliaryCarry(regs.A,regs[src]-carry,_SUB);
        regs.A -= regs[src] - carry;
    }
}
void Intel8080_Cpu::SUI()
{
    BYTE data = opcode[1];
    UpdateFlags(regs.A, regs.A - data , SIGN | ZERO | PARITY | CARRY);
    AuxiliaryCarry(regs.A,data,_SUB);
    regs.A -= data;
}
void Intel8080_Cpu::SBI()
{
    BYTE data = opcode[1];
    BYTE carry = regs.IsFlagSet(CARRY) ? 1 : 0;
    UpdateFlags(regs.A, regs.A - data - carry, SIGN | ZERO | PARITY | CARRY);
    AuxiliaryCarry(regs.A,data-carry,_SUB);
    regs.A -= data - carry;
}
// LOGICAL
void Intel8080_Cpu::ANA()
{
    BYTE src = (opcode[0] & 0x7);

    if (src == 0x06)
    {
        BYTE data = memory.ReadByte(regs.GetRegisterPair(HL));
        UpdateFlags(regs.A, (regs.A & data), SIGN | ZERO | PARITY );
        AuxiliaryCarry(regs.A,data,_AND);
        regs.SetFlag(CARRY,false);
        regs.A &= data;
    }
    else
    {

        UpdateFlags(regs.A, (regs.A & regs[src]), SIGN | ZERO | PARITY );
        AuxiliaryCarry(regs.A,regs[src],_AND);
        regs.SetFlag(CARRY,false);
        regs.A &= regs[src];

    }
}
void Intel8080_Cpu::XRA()
{
    BYTE src = (opcode[0] & 0x7);

    if (src == 0x06)
    {
        BYTE data = memory.ReadByte(regs.GetRegisterPair(HL));
        UpdateFlags(regs.A, (regs.A ^ data), SIGN | ZERO | AUXCARRY | PARITY );
        AuxiliaryCarry(regs.A,data,_XOR);
        regs.SetFlag(CARRY,false);
        regs.A ^= data;
    }
    else
    {

        UpdateFlags(regs.A, (regs.A ^ regs[src]), SIGN | ZERO | AUXCARRY | PARITY );
        AuxiliaryCarry(regs.A,regs[src],_XOR);
        regs.SetFlag(CARRY,false);
        regs.A ^= regs[src];

    }
}
void Intel8080_Cpu::ORA()
{
    BYTE src = (opcode[0] & 0x7);

    if (src == 0x06)
    {
        BYTE data = memory.ReadByte(regs.GetRegisterPair(HL));
        UpdateFlags(regs.A, (regs.A | data), SIGN | ZERO | AUXCARRY | PARITY );
        AuxiliaryCarry(regs.A,data,_OR);
        regs.SetFlag(CARRY,false);
        regs.A |= data;
    }
    else
    {

        UpdateFlags(regs.A, (regs.A | regs[src]), SIGN | ZERO | AUXCARRY | PARITY );
        AuxiliaryCarry(regs.A,regs[src],_OR);
        regs.SetFlag(CARRY,false);
        regs.A |= regs[src];

    }
}
void Intel8080_Cpu::CMP()
{
    BYTE src = (opcode[0] & 0x7);

    if (src == 0x06)
    {
        BYTE data =  memory.ReadByte(regs.GetRegisterPair(HL));
        UpdateFlags(regs.A, (regs.A - data), SIGN | ZERO | PARITY | CARRY);
        AuxiliaryCarry(regs.A,data ,_AND);
    }
    else
    {

        UpdateFlags(regs.A, (regs.A - regs[src]), SIGN | ZERO | PARITY | CARRY);
        AuxiliaryCarry(regs.A,regs[src],_CMP);

    }
}
void Intel8080_Cpu::ANI()
{
    UpdateFlags(regs.A, regs.A & opcode[1] , SIGN | ZERO | PARITY );
    AuxiliaryCarry(regs.A,opcode[1],_AND);
    regs.SetFlag(CARRY,false);
    regs.A &= opcode[1];
}
void Intel8080_Cpu::XRI()
{
    UpdateFlags(regs.A, regs.A ^ opcode[1] , SIGN | ZERO  | PARITY );
    AuxiliaryCarry(regs.A,opcode[1],_XOR);
    regs.SetFlag(CARRY,false);
    regs.A ^= opcode[1];
}
void Intel8080_Cpu::ORI()
{
    UpdateFlags(regs.A, regs.A | opcode[1] , SIGN | ZERO  | PARITY );
    AuxiliaryCarry(regs.A,opcode[1],_OR);
    regs.SetFlag(CARRY,false);
    regs.A |= opcode[1];
}
void Intel8080_Cpu::CPI()
{
    UpdateFlags(regs.A, regs.A - opcode[1], SIGN | PARITY);
    AuxiliaryCarry(regs.A,opcode[1],_CMP);
    regs.SetFlag(ZERO, (regs.A == opcode[1]));
    regs.SetFlag(CARRY, (regs.A < opcode[1]));
}
// ROTATE
void Intel8080_Cpu::RLC()
{
    BYTE bit_7 = regs.A & 0x80;
    regs.SetFlag(CARRY, bit_7 != 0);
    regs.A <<= 1;

    if(regs.IsFlagSet(CARRY))
        regs.A |= 1;
    else
        regs.A &= ~(1);
    //std::cout << "RLC"<<std::endl;
}
void Intel8080_Cpu::RRC()
{
    BYTE bit_0 = regs.A & 0x01;
    regs.SetFlag(CARRY,(bit_0 !=0));
    regs.A >>= 1;

    if(regs.IsFlagSet(CARRY))
        regs.A |= 1 << 7;
    else
        regs.A &= ~(1 << 7);
}
void Intel8080_Cpu::RAL()
{

}
void Intel8080_Cpu::RAR()
{
    BYTE carry = regs.IsFlagSet(CARRY) ? 1 : 0;
    BYTE bit_0 = regs.A & 0x01;

    regs.SetFlag(CARRY, bit_0 != 0);
    regs.A>>=1;

    if(carry)
        regs.A |= 1 << 7;
    else
        regs.A &= ~(1 << 7);


    //std::cout << "RAR"<<std::endl;
}
// SPECIALS
void Intel8080_Cpu::CMA()
{
    regs.A = ~regs.A;
}
void Intel8080_Cpu::STC()
{
    regs.SetFlag(CARRY, true);
}
void Intel8080_Cpu::CMC()
{
    if(regs.IsFlagSet(CARRY))
        regs.SetFlag(CARRY, false);
    else
        regs.SetFlag(CARRY, true);
}
void Intel8080_Cpu::DAA()
{

    if ((regs.A & 0x0f) >9 || regs.IsFlagSet(AUXCARRY))
    {
        UpdateFlags(regs.A, regs.A + 6, SIGN | ZERO | PARITY | AUXCARRY );
        regs.A += 6;
    }
    else
        regs.SetFlag(AUXCARRY , false);



    if (((regs.A & 0xf0) >> 4) >9 || regs.IsFlagSet(CARRY))
    {
        UpdateFlags(regs.A, regs.A + (6 << 4), SIGN | ZERO | PARITY | CARRY );
        regs.A += (6 << 4);
    }else
        regs.SetFlag(CARRY , false);




}
// INPUT/OUTPUT
void Intel8080_Cpu::IN()
{
    BYTE portNumber = opcode[1];



    //    regs.A = port_in[portNumber];
    if(portNumber == 1)
        regs.A = port1_in;
    if(portNumber == 2)
    {
        regs.A = port2_in;
    }
    if(portNumber == 3)
        regs.A = ((port4_hi << 8 | port4_low) << port2_out) >>8;



}
void Intel8080_Cpu::OUT()
{
    BYTE portNumber = opcode[1];
    if(portNumber == 4){
        port4_low = port4_hi;
        port4_hi = regs.A;


    }
    else if( portNumber == 2)
    {
        port2_out = regs.A & 0x07;
    }


    //port_out[portNumber] = regs.A;

}
// CONTROL
void Intel8080_Cpu::EI()
{
    interruptEnable = true;
}
void Intel8080_Cpu::DI()
{
    interruptEnable = false;
}
void Intel8080_Cpu::NOP()
{
    //	std::cout<<"nop";
}
void Intel8080_Cpu::HLT()
{
    regs.PC--;
}
