#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), keys(0)
{

    ui->setupUi(this);
    screen = new ScreenWidget(this);
    this->setCentralWidget(screen);
    emu  = new Emulator;
    cpu_thread = new QThread(this);


    connect(cpu_thread, SIGNAL(started()), emu, SLOT(execute()));
    connect(emu,SIGNAL(updateScreen(QByteArray)),screen, SLOT(updateScreen(QByteArray)));
    connect(this,SIGNAL(output(unsigned char)), emu, SLOT(input(unsigned char)));


    emu->moveToThread(cpu_thread);

    cpu_thread->start();

    QActionGroup *actionGroup = new QActionGroup(this);
    actionGroup->addAction(ui->actionScale_1x);
    actionGroup->addAction(ui->actionScale_2x);

    connect(ui->actionScale_1x,SIGNAL(triggered()), this, SLOT(onScale1x()));
    connect(ui->actionScale_2x,SIGNAL(triggered()), this, SLOT(onScale2x()));
    connect(ui->actionKeys, SIGNAL(triggered()), this, SLOT(displayKeysDialog()));

    onScale1x();


}

MainWindow::~MainWindow()
{

    delete ui;
}



void MainWindow::onScale1x()
{
    this->setFixedSize(224,256+ui->menuBar->height());
    screen->onScale1x();
}

void MainWindow::onScale2x()
{

    this->setFixedSize(224*2,256*2+ui->menuBar->height());
    screen->onScale2x();
}

void MainWindow::displayKeysDialog()
{
    KeysDialog dialog(this);
    dialog.exec();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{

    if(event->key() == Qt::Key_Return)
        keys |= 0x01;
    if(event->key() == Qt::Key_2)
        keys |= 0x02;
    if(event->key() == Qt::Key_1)
        keys |= 0x04;
    if(event->key() == Qt::Key_Space)
        keys |= 0x10;
    if(event->key() == Qt::Key_Left)
        keys |= 0x20;
    if(event->key() == Qt::Key_Right)
        keys |= 0x40;


    emit output(keys);
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{

    if(event->key() == Qt::Key_Return)
        keys &= ~0x01;
    if(event->key() == Qt::Key_2)
        keys |= 0x02;
    if(event->key() == Qt::Key_1)
        keys &= ~0x04;
    if(event->key() == Qt::Key_Space)
        keys &= ~0x10;
    if(event->key() == Qt::Key_Left)
        keys &= ~0x20;
    if(event->key() == Qt::Key_Right)
        keys &= ~0x40;


    emit output(keys);
}

void MainWindow::focusOutEvent(QFocusEvent *event)
{
   // cpu_thread->wait();
}

void MainWindow::focusInEvent(QFocusEvent *event)
{
    //cpu_thread->start();

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    cpu_thread->terminate();
}

void MainWindow::on_actionExit_triggered()
{
   QApplication::quit();
}
