#include "emulator.h"

Emulator::Emulator(QObject *parent) :
    QObject(parent), memory(0x10000), cpu(memory),input_port1(0), running(true)
{


    std::ifstream invaders_rom("invaders.rom",std::ios::binary);
        if(invaders_rom.good())
        {

            char *data;
            int length = 0;
            invaders_rom.seekg(0,std::ios::end);
            length =(int) invaders_rom.tellg();
            invaders_rom.seekg(0,std::ios::beg);

            data = new char[length];
            invaders_rom.read(data,length);
            memory.LoadMemory((BYTE*)data,length);
        }else
        {
            QMessageBox msgBox;
            msgBox.setWindowIcon(QIcon(":/icon.png"));
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setText("Unable to load file: invaders.rom");
            msgBox.exec();

            exit(1);
        }



}

void Emulator::execute()
{
    QElapsedTimer time;


    while(running)
    {
        time.start();
        QCoreApplication::processEvents();



        cpu.Run(17066);
        cpu.InterruptRequest(RST1); // middle screen interrupt
        cpu.Input(input_port1);
        cpu.Run(17066);
        cpu.InterruptRequest(RST2); // vblank interrupt
        emit updateScreen(QByteArray((char*)memory.GetByte(0x2400),0x1bff));
        unsigned long sleep = 1000/60 - time.elapsed();
        //  qDebug() << sleep;
        QThread::msleep(sleep);



    }

}

void Emulator::input(unsigned char in)
{
    input_port1 = in;
}







