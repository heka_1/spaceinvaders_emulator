#ifndef INTEL8080_REGS_H
#define INTEL8080_REGS_H
#include "Types.h"
#include <cassert>
struct Intel8080_Regs
{

    BYTE A, B, C, D, E, H, L, F;

    WORD PC, SP;

    UINT32 temp;
    BYTE tmp;

    void SetRegisterPair(int rp, WORD data);
    WORD GetRegisterPair(int rp);
    BYTE& operator[] (const int index);

    void SetFlag(FLAG flag, bool state)
    {
        F = (state) ? (F | flag) : (F & ~(flag)) ;
    }
    bool IsFlagSet(FLAG flag)
    {
         return ((F & flag) != 0);
    }

};

#endif // INTEL8080_REGS_H
