#include "intel8080_regs.h"

BYTE& Intel8080_Regs::operator[] (const int index)
    {
        switch(index)
        {

            case 0:
                return B;
            case 1:
                return C;
            case 2:
                return D;
            case 3:
                return E;
            case 4:
                return H;
            case 5:
                return L;
            case 7:
                return A;
            default:
                assert(true);
                return tmp;
                break;

        };

    }
    WORD Intel8080_Regs::GetRegisterPair(int rp)
    {
        if(rp == 0)
            return B << 8 | C;
        else if( rp == 1)
            return D << 8 | E;
        else if( rp == 2)
            return H << 8 | L;
        else
            return 0;
    }

    void Intel8080_Regs::SetRegisterPair(int rp, WORD data)
    {


        if(rp == 0)
        {
            B = (BYTE)(data >> 8);
            C = (BYTE)data;
        }
        else if( rp == 1)
        {
            D = (BYTE)(data >> 8);
            E = (BYTE)data;
        }
        else if( rp == 2)
        {
            H = (BYTE)(data >> 8);
            L = (BYTE)data;
        }

    }

