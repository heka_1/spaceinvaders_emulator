#ifndef KEYSDIALOG_H
#define KEYSDIALOG_H

#include <QDialog>

namespace Ui {
class KeysDialog;
}

class KeysDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit KeysDialog(QWidget *parent = 0);
    ~KeysDialog();
    
private:
    Ui::KeysDialog *ui;
};

#endif // KEYSDIALOG_H
