#ifndef MEMORY_H
#define MEMORY_H
#include "Types.h"
#include <iostream>
class Memory
{
public:
    Memory(UINT32 size);

    ~Memory(void);

    BYTE ReadByte(WORD address) const;
    WORD ReadWord(WORD address) const;
    void WriteByte(WORD address, BYTE data);
    void WriteWord(WORD address, WORD data);

    void LoadMemory(BYTE* data, WORD length);
    void LoadMemoryFromAddress(WORD address,BYTE* data, WORD length);
    BYTE* GetByte(int address) { return &memory[address]; }
    void Dump();
    void ClearVram();
private:
    UINT32 memorySize;
    BYTE* memory;
};

#endif // MEMORY_H
