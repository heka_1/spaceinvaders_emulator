#ifndef TYPES_H
#define TYPES_H

typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned int UINT32;

enum { RST0 = 0xc7, RST1 = 0xcf, RST2 = 0xd7} ;
enum FLAG { SIGN = 0x80, ZERO = 0x40, AUXCARRY = 0x10, PARITY = 0x04, CARRY = 0x01 };

#endif // TYPES_H
