#-------------------------------------------------
#
# Project created by QtCreator 2013-04-13T18:45:29
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SpaceInvaders
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    emulator.cpp \
    screenwidget.cpp \
    intel8080_cpu.cpp \
    memory.cpp \
    intel8080_regs.cpp \
    keysdialog.cpp

HEADERS  += mainwindow.h \
    emulator.h \
    screenwidget.h \
    intel8080_cpu.h \
    memory.h \
    intel8080_regs.h \
    Types.h \
    keysdialog.h

FORMS    += mainwindow.ui \
    keysdialog.ui

RESOURCES += \
    icon.qrc \
    roms.qrc

DISTFILES +=
