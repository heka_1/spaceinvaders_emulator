#ifndef SCREENWIDGET_H
#define SCREENWIDGET_H
#include <QPainter>
#include <QGLWidget>
#include <QBrush>
#include <QDebug>
#include <QPaintEvent>
#include <QImage>
#include "Types.h"


class ScreenWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit ScreenWidget(QWidget *parent = 0);
protected:
    void paintEvent(QPaintEvent *event);

signals:
    
public slots:
    void onScale1x();
    void onScale2x();
    void updateScreen(QByteArray pixels);
private:
    int scale;
    QByteArray screen_pixels;
    QImage image;

};

#endif // SCREENWIDGET_H
