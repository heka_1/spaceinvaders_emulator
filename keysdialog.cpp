#include "keysdialog.h"
#include "ui_keysdialog.h"

KeysDialog::KeysDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::KeysDialog)
{
    ui->setupUi(this);
    setFixedSize(291,176);
}

KeysDialog::~KeysDialog()
{
    delete ui;
}
